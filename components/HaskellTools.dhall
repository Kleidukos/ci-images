let
  CF = ../deps/Containerfile.dhall

let
  outDir: Text = "/opt/toolchain"
let
  binDir: Text = "${outDir}/bin"

let
  build: CF.Type =
      CF.run "build haskell tools"
      [ "mkdir -p ${outDir}/store"
      , "$CABAL user-config update"
      , "$CABAL v2-update"
         -- We must override store-dir lest data files end up in
         -- /root/.cabal, which isn't accessible to the build user
      , ''
        $CABAL \
          --store-dir=${outDir}/store \
          v2-install \
          --constraint='happy ^>= 1.20' \
          --with-compiler=$GHC \
          --enable-static \
          --install-method=copy \
          --installdir=${binDir} \
          hscolour happy alex
        ''
      ]
    # CF.env (toMap
      { HAPPY = "${binDir}/happy"
      , ALEX = "${binDir}/alex"
      , HSCOLOUR = "${binDir}/HsColour"
      })

in
{
    build = build
}
