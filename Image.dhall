--| A description of a Docker image 

let
  CF = ./deps/Containerfile.dhall

let
  --| The name a of a Docker image
  ImageName = Text

let
  type: Type =
  { name: ImageName
  , runnerTags: List Text
  , jobStage: Text
  , needs: List ImageName
  , image: CF.Type
  }
in
{ Type = type
, default =
  { needs = [] : List ImageName
  , jobStage = "build"
  }
, ImageName = ImageName
}